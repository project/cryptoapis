CryptoAPIs SDK for all Exchanges, Bitcoin, Litecoin, Bitcoin Cash, Doge Coin, Dash and Ethereum endpoints.

=========================================
== Dependencies and Other Requirements ==
=========================================
- Libraries API 2.x - https://drupal.org/project/libraries
- Cryptoapis SDK for PHP 2.x - https://github.com/vispamir/cryptoapis
- PHP 5.3.3+ is required. The Cryptoapis SDK will not work on earlier versions.

==================
== Installation ==
==================
1) Install Libraries version 2.x from http://drupal.org/project/libraries.

2) Install the Cryptoapis SDK for PHP.
  a) If you have drush, you can install the SDK with this command (executed
    from the root folder of your Drupal codebase):
    drush make --no-core sites/all/modules/cryptoapis/cryptoapis.make
  b) If you don't have drush, download the SDK from here:
    https://github.com/vispamir/cryptoapis/archive/master.zip
    Extract that zip file into your Drupal codebase's
    sites/all/libraries/cryptoapis folder such that the path to cryptoapis-autoloader.php
    is: sites/all/libraries/cryptoapis/cryptoapis-autoloader.php

IN CASE OF TROUBLE DETECTING THE Cryptoapis SDK LIBRARY:
Ensure that the cryptoapis folder itself, and all the files within it, can be read
by your webserver. Usually this means that the user "apache" (or "_www" on OSX)
must have read permissions for the files, and read+execute permissions for all
the folders in the path leading to the cryptoapis files.
