core = 7.x
api = 2

libraries[cryptoapis][download][type]= "get"
libraries[cryptoapis][download][url] = "https://github.com/vispamir/cryptoapis/archive/master.zip"
libraries[cryptoapis][directory_name] = "cryptoapis"
libraries[cryptoapis][destination] = "libraries"
