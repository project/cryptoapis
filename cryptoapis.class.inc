<?php

/**
 * @file
 * Custom class from CryptoApis.
 */

use \RestApis\Factory;

class cryptoapis extends Factory {

  /**
   * cryptoapis constructor.
   * @param $apiKey
   * @param int $apiVersion
   * @throws Exception
   */
  public function __construct($apiKey, $apiVersion = 1) {
    @set_exception_handler(array($this, 'exception_handler'));

    // Call factory constructor
    parent::__construct($apiKey, $apiVersion);
  }

  /**
   * @param $exception
   */
  public function exception_handler($exception) {
    watchdog(
      'cryptoapis',
      'Not available service: @exception',
      array('@exception' => $exception->getMessage()),
      WATCHDOG_ERROR
    );
  }
}
