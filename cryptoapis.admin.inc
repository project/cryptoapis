<?php

/**
 * @file
 * Administration form setup for Cryptoapis.
 */

/**
 * Builds the Settings form.
 */
function cryptoapis_setting_form($form, &$form_state) {

  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API configs'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
  );

  $form['api']['api_key'] = array(
    '#type' => 'textfield',
    '#title' => 'API Key',
    '#description' => 'Your api-key on cryptoapis.io',
    '#default_value' => variable_get('api_key', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
